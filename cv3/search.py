import sys
import sqlite3
import json

conn = sqlite3.connect( 'scorelib.dat' )
#sys.argv.append("Bach") #for debug
if sys.argv != None and len(sys.argv) >=2:
    composerName = sys.argv[1]
    composersQuery = "SELECT DISTINCT p.id, p.name FROM person p INNER JOIN score_author sa ON sa.composer = p.id WHERE p.name LIKE ?"
    scoresQuery = "SELECT s.genre, s.key, s.incipit, s.year FROM score s INNER JOIN score_author sa ON sa.score = s.id WHERE sa.composer = ?"
    cursor = conn.cursor()
    composersResult = cursor.execute(composersQuery, ('%' + composerName + '%',))
    data = {}
    data["composers"] = []
    composerRow = composersResult.fetchone()
    while(composerRow is not None):
        composerData = {}
        composerData["name"] = composerRow[1]
        composerData["scores"] = []
        scoreResult = conn.cursor().execute(scoresQuery, (composerRow[0],))
        scoreRow = scoreResult.fetchone()
        while(scoreRow is not None):
            scoreData = {}
            scoreData["genre"] = scoreRow[0]
            scoreData["key"] = scoreRow[1]
            scoreData["incipit"] = scoreRow[2]
            scoreData["year"] = scoreRow[3]
            composerData["scores"].append(scoreData)
            scoreRow = scoreResult.fetchone()
        data["composers"].append(composerData)
        composerRow = composersResult.fetchone()
    json.dump(data, sys.stdout, indent=4)
