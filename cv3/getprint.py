import sys
import sqlite3
import json

conn = sqlite3.connect( 'scorelib.dat' )
#sys.argv.append("10") #for debug
if sys.argv != None and len(sys.argv) >=2:
    printNumber = sys.argv[1]
    query = "SELECT p.name FROM print pr INNER JOIN edition e ON pr.edition = e.id INNER JOIN score s ON e.score = s.id INNER JOIN score_author sa ON s.id = sa.score INNER JOIN person p ON sa.composer = p.id WHERE pr.id = ?"
    cursor = conn.cursor()
    result = cursor.execute(query, (printNumber,))
    data = {}
    data["composers"] = []
    row = result.fetchone()
    while(row is not None):
        data["composers"].append(row[0])
        row = result.fetchone()
    json.dump(data, sys.stdout, indent=4)
