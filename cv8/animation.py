import pygame, random
from pygame import display, draw, time, event

# constants
FPS = 60
SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600
CIRCLE_MIN_RADIUS = 10
CIRCLE_MAX_RADIUS = 50

# classes
class Circle:
    def __init__(self, position, radius, color, width):
        self.position = position
        self.radius = radius
        self.start_radius = radius
        self.color = color
        self.width = width

# methods
def create_circle():
    position_x = random.randint(CIRCLE_MAX_RADIUS, SCREEN_WIDTH - CIRCLE_MAX_RADIUS)
    position_y = random.randint(CIRCLE_MAX_RADIUS, SCREEN_HEIGHT - CIRCLE_MAX_RADIUS)
    radius = random.randint(CIRCLE_MIN_RADIUS, CIRCLE_MAX_RADIUS)
    color = [random.randint(0,255),random.randint(0,255),random.randint(0,255)]
    return Circle([position_x, position_y], radius, color, max(CIRCLE_MIN_RADIUS, int(radius / 2)))

def update_circles(source_circles):
    indices_to_remove = [] # descending
    for index in reversed(range(len(source_circles))):
        c = source_circles[index]
        if c.radius - c.start_radius >= FPS:
            indices_to_remove.append(index)
        else:
            c.radius = c.radius + 1
    for i in indices_to_remove:
        source_circles.pop(i)
    return

# app
pygame.init()
screen = display.set_mode([SCREEN_WIDTH, SCREEN_HEIGHT])
display.set_caption("Press ESC to exit")
clock = time.Clock()

circles = []

while (True):
    clock.tick(FPS)

    e = event.poll()
    if e.type == pygame.QUIT or (e.type == pygame.KEYDOWN and e.key == pygame.K_ESCAPE):
        break

    screen.fill([0, 0, 0])
    update_circles(circles)
    circles.append(create_circle())
    for circle in circles:
        draw.circle(screen, circle.color, circle.position, circle.radius, circle.width)
    
    display.flip()




