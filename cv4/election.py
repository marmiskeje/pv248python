import json
import random
from math import pi
from bokeh.models import Legend
from bokeh.plotting import figure, show, output_file

def getRandomColor():
    return '#{:06x}'.format(random.randint(0, 256**3))

def exercise1():
    data = json.load(open('election.json', 'r', encoding='utf-8'))
    plot = figure(plot_width = 1800, plot_height = 900, title = "Bar plot - exercise1")
    legendItems = []
    for item in data:
        color = item['color'] if 'color' in item else getRandomColor()
        legend = item['short'] if 'short' in item else item['name']
        b = plot.vbar(x = item['number'], top = item['share'], width = 0.7, fill_color = color )
        legendItems.append((legend, [b]))
    output_file("exercise1.html")
    plot.add_layout(Legend(items=legendItems, location=(0, 0)), 'right')
    show(plot)
    return

def exercise2():
    data = json.load(open('election.json', 'r', encoding='utf-8'))
    plot = figure(plot_width = 1800, plot_height = 900, title = "Bar plot - exercise2")
    plot.legend.location = "top_left"
    plot.legend.label_text_font_size ='10pt'
    rest_share = 0.0
    counter = 1
    legendItems = []
    for item in data:
        if item['share'] >= 1.0:
            color = item['color'] if 'color' in item else getRandomColor()
            legend = item['short'] if 'short' in item else item['name']
            b = plot.vbar(x = counter, top = item['share'], width = 0.7, fill_color = color)
            counter = counter + 1
            legendItems.append((legend, [b]))
        else:
            rest_share = rest_share + item['share']
    if rest_share > 0:
        b = plot.vbar(x = counter, top = rest_share, width = 0.7, fill_color = 'darkgray' )
        legendItems.append(('ostatní', [b]))
    output_file("exercise2.html")
    plot.add_layout(Legend(items=legendItems, location=(0, 0)), 'right')
    show(plot)
    return

def exercise3():
    data = json.load(open('election.json', 'r', encoding='utf-8'))
    plot = figure(plot_width = 800, plot_height = 800, title = "Pie chart - exercise3")
    previous_end_angle = 0.0
    for item in data:
        color = item['color'] if 'color' in item else getRandomColor()
        legend = item['short'] if 'short' in item else item['name']
        start_angle = previous_end_angle
        end_angle = start_angle + ((item['share'] / 100.0) * 2*pi)
        w = plot.wedge(x=0, y=0, radius = 1, start_angle = [start_angle], end_angle = [end_angle], color = color)
        previous_end_angle = end_angle
    output_file("exercise3.html")
    show(plot)
    return

exercise1()
exercise2()
exercise3()