import os
import re  # regular expressions
import sqlite3

# This is a base class for objects that represent database items. It implements
# the store() method in terms of fetch_id and do_store, which need to be
# implemented in every derived class (see Person below for an example).

class DBItem:
    def __init__( self, conn ):
        self.id = None
        self.cursor = conn.cursor()

    def store( self ):
        self.fetch_id()
        if ( self.id is None ):
            self.do_store()
            self.cursor.execute( "select last_insert_rowid()" )
            self.id = self.cursor.fetchone()[ 0 ]

class Print():
    def __init__(self,conn):
        self.id = None
        self.cursor = conn.cursor()
        self.partiture = None
        self.editionId = None

    def store( self ):
        print ("storing print '%s' '%s' '%s'" % (self.id, self.partiture, self.editionId))
        self.cursor.execute( "insert into print (id, partiture, edition) values (?, ?, ?)",
                             ( self.id, self.partiture, self.editionId ) )

class EditionAuthor(DBItem):
    def __init__(self, conn, editionId, editorId):
        super().__init__(conn)
        self.editionId = editionId
        self.editorId = editorId

    def fetch_id( self ):
        self.cursor.execute( "select id from edition_author where edition = ? and editor = ?", (self.editionId, elf.editorId,) )

        res = self.cursor.fetchone()
        if not res is None:
            self.id = res[ 0 ]

    def do_store( self ):
        print ("storing edition_author '%s' '%s'" % (self.editionId, self.editorId))
        self.cursor.execute( "insert into edition_author (edition, editor) values (?, ?)",
                             ( self.editionId, self.editorId ) )

class Edition(DBItem):
    def __init__(self,conn):
        super().__init__(conn)
        self.scoreId = None
        self.name = None
        self.year = None
    
    def fetch_id( self ):
        self.cursor.execute( "select id from edition where score = ? and name = ? and year = ?", (self.scoreId, self.name, self.year,) )

        res = self.cursor.fetchone()
        if not res is None:
            self.id = res[ 0 ]

    def do_store( self ):
        print ("storing edition '%s' '%s'" % (self.name, self.year))
        self.cursor.execute( "insert into edition (score, name, year) values (?, ?, ?)",
                             ( self.scoreId, self.name, self.year ) )

class Voice(DBItem):
    def __init__(self, conn):
        super().__init__(conn)
        self.number = None
        self.scoreId = None
        self.name = None

    def fetch_id( self ):
        self.cursor.execute( "select id from voice where number = ? and score = ? and name = ?", (self.number, self.scoreId, self.name,) )

        res = self.cursor.fetchone()
        if not res is None:
            self.id = res[ 0 ]

    def do_store( self ):
        print ("storing voice '%s' '%s'" % (self.number, self.name))
        self.cursor.execute( "insert into voice (number, score, name) values (?, ?, ?)",
                             ( self.name, self.scoreId, self.name ) )

class ScoreAuthor(DBItem):
    def __init__(self, conn, scoreId, composerId):
        super().__init__(conn)
        self.scoreId = scoreId
        self.composerId = composerId

    def fetch_id( self ):
        self.cursor.execute( "select id from score_author where score = ? and composer = ?", (self.scoreId, self.composerId,) )

        res = self.cursor.fetchone()
        if not res is None:
            self.id = res[ 0 ]

    def do_store( self ):
        print ("storing score_author '%s' '%s'" % (self.scoreId, self.composerId))
        self.cursor.execute( "insert into score_author (score, composer) values (?, ?)",
                             ( self.scoreId, self.composerId ) )

class Score (DBItem):
    def __init__(self, conn):
        super().__init__(conn)
        self.genre = None
        self.key = None
        self.incipit = None
        self.year = None
        self.publicationYear = None
        self.composers = []
        self.editors = []
        self.voices = []
        self.edition = None
        self.print = None

    def fetch_id( self ):
        self.cursor.execute( "select id from score where genre = ? and key = ? and incipit = ? and year = ?", (self.genre, self.key, self.incipit, self.year,) )

        res = self.cursor.fetchone()
        if not res is None:
            self.id = res[ 0 ]

    def do_store( self ):
        print ("storing score '%s' '%s' '%s' '%s'" % (self.genre, self.key, self.incipit, self.year))
        self.cursor.execute( "insert into score (genre, key, incipit, year) values (?, ?, ?, ?)",
                             ( self.genre, self.key, self.incipit, self.year ) )

# Example of a class which represents a single row of a single database table.
# This is a very simple example, since it does not contain any references to
# other objects.

class Person( DBItem ):
    def __init__( self, conn, string ):
        super().__init__( conn )
        self.born = self.died = None
        self.name = re.sub( '\([0-9/+-]+\)', '', string ).strip()
        # NB. The code below was part of the exercise (extracting years of birth & death
        # from the string).
        m = re.search( "([0-9]+)--([0-9]+)", string )
        if not m is None:
            self.born = int( m.group( 1 ) )
            self.died = int( m.group( 2 ) )

    # Update born/died if the name is already present but has null values for
    # those fields. We assume that names are unique (not entirely true in practice).
    def fetch_id( self ):
        self.cursor.execute( "select id, born, died from person where name = ?", (self.name,) )

        # NB. The below lines had a bug in the original version of
        # scorelib-import.py (which however only becomes relevant when you
        # start implementing the Score class).
        res = self.cursor.fetchone()
        if not res is None: # born/died update should be done inside this if
            self.id = res[ 0 ]
            if (res[1] is None and self.born is not None) or (res[2] is None and self.died is not None):
                self.cursor.execute("update person set born = ?, died = ? where id = ?", (self.born, self.died, self.id))

    def do_store( self ):
        print ("storing person '%s'" % self.name)
        self.cursor.execute( "insert into person (name, born, died) values (?, ?, ?)",
                             ( self.name, self.born, self.died ) )

# NB. Everything below this line was part of the exercise.

# returns true, when score should be created
def processScore(k, v, score):
    if k == 'Print Number':
        if v.strip() != "":
            print = Print(conn)
            print.id = int(v.strip())
            score.print = print
    elif k == 'Composer':
        for c in v.split(';'):
            if c is not None and c.strip() != "":
                score.composers.append(Person(conn, c.strip()))
    elif k == 'Editor':
        for e in v.split(','):
            if e is not None and e.strip() != "":
                score.editors.append(Person(conn, e.strip()))
    elif k == 'Composition Year':
        if v.strip() != "":
            score.year = v.strip()
    elif k == 'Publication Year':
        if v.strip() != "":
            score.publicationYear = v.strip()
    elif k == 'Genre':
        if v.strip() != "":
            score.genre = v.strip()
    elif k == 'Key':
        if v.strip() != "":
            score.key = v.strip().lower()
    elif k == 'Edition':
        if v.strip() != "":
            edition = Edition(conn)
            edition.year = score.publicationYear
            edition.name = v.strip()
            score.edition = edition
    elif "Voice" in k:
        if v.strip() != "":
            toAdd = Voice(conn)
            toAdd.name = v.strip()
            toAdd.number = k.split(" ")[1]
            score.voices.append(toAdd)
    elif k == 'Partiture':
        val = v.strip().lower()
        if (val != "" and score.print != None):
            if val == "yes":
                score.print.partiture = "Y"
            elif val == "no":
                score.print.partiture = "N"
    elif k == 'Incipit':
        if v.strip() != "":
            score.incipit = v.strip()
        return True
    return False

# Database initialisation: sqlite3 scorelib.dat ".read scorelib.sql"
os.remove("scorelib.dat")
conn = sqlite3.connect( 'scorelib.dat' )
f = open("scorelib.sql", "r")
conn.executescript(f.read())
rx = re.compile( r"(.*):(.*)" )
score = Score(conn)
for line in open( 'scorelib.txt', 'r', encoding='utf-8' ):
    m = rx.match( line )
    if m is None: continue
    if processScore(m.group( 1 ), m.group( 2 ), score):
        score.store()
        for c in score.composers:
            c.store()
            a = ScoreAuthor(conn, score.id, c.id)
            a.store()
        for e in score.editors:
            e.store()
        for v in score.voices:
            v.scoreId = score.id
            v.store()
        if score.edition != None:
            e = score.edition
            e.scoreId = score.id
            e.store()
            for se in score.editors:
                se.edition = e.id
                se.store()
        if score.print != None:
            if score.edition != None:
                score.print.editionId = score.edition.id
            if score.print.partiture == None:
                score.print.partiture = "P"
            score.print.store()
        score = Score(conn)
conn.commit()
