from http.server import BaseHTTPRequestHandler,HTTPServer


HOST_NAME = 'localhost'
PORT_NUMBER = 8000


class MyHTTPRequestHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200, 'OK')
        self.send_header('Content-Type', 'text/html; charset=utf-8')
        self.end_headers()
        response = '<!DOCTYPE html><html><head><meta charset="UTF-8" /><title>Response from python</title></head><body></body>'
        response = '<h1>PV248 - Python HTTP server</h1>'
        response = response + '<p>You asked for ' + self.path +  '</p>'
        response = response + '</html>'
        self.wfile.write(bytes(response, 'utf-8'))
        return

if __name__ == '__main__':
    server_class = HTTPServer
    httpd = server_class((HOST_NAME, PORT_NUMBER), MyHTTPRequestHandler)
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    httpd.server_close()