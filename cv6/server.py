import json
import sqlite3
from http.server import BaseHTTPRequestHandler,HTTPServer
from urllib.parse import urlparse,parse_qs


HOST_NAME = 'localhost'
PORT_NUMBER = 8000
conn = sqlite3.connect( 'scorelib.dat' )

class MyHTTPRequestHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        urlparse_result = urlparse(self.path)
        query_string_dict = parse_qs(urlparse_result.query)
        content_type = "text/html; charset=utf-8"
        response = ""
        if urlparse_result.path == "/result" and "q" in query_string_dict and "f" in query_string_dict:
            data = self.getData(query_string_dict["q"][0])
            self.send_response(200, 'OK')
            if query_string_dict["f"][0] == "html":
                for c in data["composers"]:
                    response = response + "<h1>Composer: %s</h1>" % (c["name"],)
                    response = response + "<h2>Scores:</h2>"
                    response = response + "<ul>"
                    for s in c["scores"]:
                        response = response + "<li>Genre: %s, Key: %s, Incipit: %s, Year: %s</li>" % (s["genre"],s["key"],s["incipit"],s["year"],)
                    response = response + "</ul>"
            else:
                content_type = 'application/json'
                response = json.dumps(data)
        else:
            response = """<!DOCTYPE html><html><head><meta charset="UTF-8" /><title>Response from python</title></head>
            <body>
            <form action="/result" method="get">
            Composer name:<br />
            <input type="text" name="q" /><br />
            Format:<br />
            <select name="f">
                <option value="json">Json</option>
                <option value="html" selected>Html</option>
            </select>
            <br />
            <br />
            <input type="submit" value="Submit" />
            </form>
            </body>
            </html>"""
        self.send_response(200, 'OK')
        self.send_header('Content-Type', content_type)
        self.end_headers()
        self.wfile.write(bytes(response, 'utf-8'))
        return
    
    def getData(self, composerName):
        composersQuery = "SELECT DISTINCT p.id, p.name FROM person p INNER JOIN score_author sa ON sa.composer = p.id WHERE p.name LIKE ?"
        scoresQuery = "SELECT s.genre, s.key, s.incipit, s.year FROM score s INNER JOIN score_author sa ON sa.score = s.id WHERE sa.composer = ?"
        cursor = conn.cursor()
        composersResult = cursor.execute(composersQuery, ('%' + composerName + '%',))
        data = {}
        data["composers"] = []
        composerRow = composersResult.fetchone()
        while(composerRow is not None):
            composerData = {}
            composerData["name"] = composerRow[1]
            composerData["scores"] = []
            scoreResult = conn.cursor().execute(scoresQuery, (composerRow[0],))
            scoreRow = scoreResult.fetchone()
            while(scoreRow is not None):
                scoreData = {}
                scoreData["genre"] = scoreRow[0]
                scoreData["key"] = scoreRow[1]
                scoreData["incipit"] = scoreRow[2]
                scoreData["year"] = scoreRow[3]
                composerData["scores"].append(scoreData)
                scoreRow = scoreResult.fetchone()
            data["composers"].append(composerData)
            composerRow = composersResult.fetchone()
        return data

if __name__ == '__main__':
    server_class = HTTPServer
    httpd = server_class((HOST_NAME, PORT_NUMBER), MyHTTPRequestHandler)
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    httpd.server_close()