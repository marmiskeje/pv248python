"""Cv1"""
from collections import Counter
import re

class DataExtractor:
    def __init__(self):
        self.maxYearRegex = re.compile(r"\d+")

    def extract_century(self, dateString):
        maxNumber = None
        century = None
        m = self.maxYearRegex.findall(dateString)
        if m is not None and len(m) > 0:
            maxNumber = max(map(int, m))
            if len(m) == 1: # might be century
                if "century" in dateString:
                    century = maxNumber
            if century is None:
                century = ((maxNumber - 1) // 100) + 1
        return century

class App:
    def main(self):
        dataExtractor = DataExtractor()
        composerRegex = re.compile(r"Composer: (.*)")
        composedYearRegex = re.compile(r"Composition Year: (.*)")
        keyRegex = re.compile(r"Key: (.*)")
        titleRegex = re.compile(r"Title: (.*)")
        f = open(r'scorelib.txt', 'r', encoding='utf8')
        composerPiecesCount = Counter()
        centuriesCount = Counter()
        keysCount = Counter()
        for line in f:
            m = composerRegex.match(line)
            if m is not None:
                matchedValue = m.group(1).strip()
                if matchedValue:
                    composerPiecesCount[matchedValue] += 1
            m = composedYearRegex.match(line)
            if m is not None and m.group(1):
                matchedValue = m.group(1).strip()
                if matchedValue:
                    century = dataExtractor.extract_century(matchedValue)
                    if century is not None:
                        centuriesCount[century] += 1
            m = keyRegex.match(line)
            if m is not None:
                matchedValue = m.group(1).strip()
                if matchedValue:
                    split = matchedValue.split(";")
                    for s in split:
                        keyValue = s.lower().strip()
                        if keyValue == "c minor":
                            keysCount[keyValue] +=1
            else:
                m = titleRegex.match(line)
                if m is not None and "c minor" in line.lower():
                    keysCount["c minor"] += 1

        print("****how many pieces by each composer?****\n")
        for composer in composerPiecesCount:
            print(composer + ": " + str(composerPiecesCount[composer]))
        print("\n****how many pieces composed in a given century****\n")
        for century in sorted(centuriesCount):
            print(self.__formatCenturyToString(century) + ": " + str(centuriesCount[century]))
        print("\n****how many in the key c minor?****\n")
        for key in keysCount:
            print(key + ": " + str(keysCount[key]))

    def __formatCenturyToString(self, century):
        centuries = {
            1: "st",
            2: "nd",
            3: "rd",
        }
        return str(century) + centuries.get(century, "th") + " century"

App().main()
