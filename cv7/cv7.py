import numpy
import re
from collections import Counter

def part1():
    matrix = numpy.loadtxt("matrix.txt", dtype=int)
    determinant = numpy.linalg.det(matrix)
    print("Matrix:")
    print(matrix)
    print("Determinant:")
    print(determinant)
    print("Inverse matrix:")
    if determinant != 0:
        inverse_matrix = numpy.linalg.inv(matrix)
        print(inverse_matrix)
    else:
        print("Not exists.")
    return

def part2():
    matrix = numpy.loadtxt("lineareq.txt", dtype=int)
    print("Matrix (A|b):")
    print(matrix)
    a = matrix[0:matrix.shape[0], 0:(matrix.shape[1]-1)]
    print("Part A:")
    print(a)
    b = matrix[0:matrix.shape[0],(matrix.shape[1]-1):matrix.shape[1]]
    print("Part B:")
    print(b)
    solution = numpy.linalg.solve(a, b)
    print("Solution")
    print(solution)
    return

def part3():
    variables_occurences = {} # value = (index, value)
    a = []
    b = []
    row = 0
    matrix_size_rows = 0
    print("Input:")
    with open("lineareq_human.txt", encoding="utf-8") as f:
        lines = f.readlines()
        row = 0
        for l in lines:
            print(l.strip())
            parse_result = parseExpression(l)
            for v in parse_result[0]:
                if not v in variables_occurences:
                    variables_occurences[v] = []
                variables_occurences[v].append((row, parse_result[0][v]))
            b.append(parse_result[1])
            row = row + 1
            matrix_size_rows = matrix_size_rows + 1
    matrix_size_columns = len(variables_occurences)
    a = [[0 for x in range(matrix_size_columns)] for y in range(matrix_size_rows)]
    column = 0
    for vo in variables_occurences:
        for o in variables_occurences[vo]:
            row = o[0]
            value = o[1]
            a[row][column] = value
        column = column + 1

    solution = numpy.linalg.solve(a, b)
    print("Solution:")
    column = 0
    for vo in variables_occurences:
        print(vo + " = " + str(solution[column]))
        column = column + 1
    return

def parseExpression(e):
    equal_split = e.split("=")
    variables = Counter()
    b = 0
    if len(equal_split) == 2:
        left_side_split = re.split("\+", equal_split[0].strip())
        negate_second_value = False
        if len(left_side_split) == 1:
            left_side_split = re.split("\-", equal_split[0].strip())
            negate_second_value = len(left_side_split) == 2
        value_counter = 0
        for s in left_side_split:
            value_counter = value_counter + 1
            left_side = parseLeftSideExpression(s.strip())
            b = b + (left_side[2] * -1)
            if negate_second_value and value_counter == 2:
                variables[left_side[1]] = variables[left_side[1]] - left_side[0]
            else:
                variables[left_side[1]] = variables[left_side[1]] + left_side[0]
        b = b + int(equal_split[1].strip())
    return (variables, b)

def parseLeftSideExpression(e):
    literals = e.split(" ")
    literals = [x.strip() for x in literals if x.strip() != ""] #do strip for every item and remove empty
    b = 0
    const_value = 0
    variable = None
    if len(literals) == 1:
        if literals[0].isdigit(): # only constant, we must transfer to the right side
            b = int(literals[0])
        else:
            const_value = 1
            variable = literals[0]
    else:
        const_value = int(literals[0])
        variable = literals[1]
    return (const_value, variable, b)

part1()
part2()
part3()